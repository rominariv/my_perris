from django.db import models

# Create your models here.

class Registro(models.Model):
    nombre = models.CharField(max_length=100)
    run = models.CharField(max_length=12)
    fecha_naci = models.CharField(max_length=20)
    email = models.CharField(max_length=100)
    celular = models.CharField(max_length=20)
    region = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    casa = models.CharField(max_length=50)
    contrasenia = models.CharField(max_length=10)

class Mascota(models.Model):

    nombre = models.CharField(max_length=100)
    raza = models.CharField(max_length=100)
    estado = models.CharField(max_length=50)
    imagen = models.ImageField(upload_to="fotos/")
    comentario = models.CharField(max_length=50)

