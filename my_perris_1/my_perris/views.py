from django.shortcuts import render
from django.http import HttpResponse
from .models import Registro
from .models import Mascota
from django.shortcuts import redirect
# importar user
from django.contrib.auth.models import User
# sistema de autenticación
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib import admin



def index(request):
    return render(request, 'index.html', {})


def registro(request):
    return render(request, 'registro.html', {})


def login(request):
    return render(request, 'login.html', {})


def login_iniciar(request):
    email = request.POST.get('email',False)
    contrasenia = request.POST.get('contrasenia',False)
    user = authenticate(request, username=email, password=contrasenia)
    print (email,contrasenia)
    print (user)
    if user is not None:
        auth_login(request, user)
        """request.session['email'] = user.email"""
        return redirect("vista_modificar")
    else:
        return redirect("login")


def mascotas(request):
    return render(request, 'mascotas.html', {'elementos': Mascota.objects.all()})


def adoptantes(request):
    return render(request, 'adoptantes.html', {})


def crear(request):
    nombre = request.POST.get('nombre', '')
    run = request.POST.get('run', '')
    fecha_naci = request.POST.get('fecha_naci', '')
    email = request.POST.get('email', '')
    celular = request.POST.get('celular', 0)
    region = request.POST.get('region', '')
    ciudad = request.POST.get('ciudad', '')
    casa = request.POST.get('casa', '')
    contrasenia = request.POST.get('contrasenia', '')
    registro = Registro(nombre=nombre, run=run, fecha_naci=fecha_naci, email=email,
                        celular=celular, region=region, ciudad=ciudad,
                        casa=casa, contrasenia=contrasenia)
    registro.save()
    return HttpResponse("Su cuenta se creo correctamente")


def crear_mascota(request):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    estado = request.POST.get('estado', '')
    imagen = request.FILES.get('imagen', '')
    comentario = request.POST.get('comentario', '')
    mascota = Mascota(nombre=nombre, raza=raza, estado=estado,
                      imagen=imagen, comentario=comentario)
    mascota.save()
    return redirect('mascotas')


def eliminar(request, id):
    mascota = Mascota.objects.get(pk=id)
    mascota.delete()
    return redirect('mascotas')


def modificar(request, id):
    nombre = request.POST.get('nombre', '')
    raza = request.POST.get('raza', '')
    estado = request.POST.get('estado', '')
    imagen = request.FILES.get('imagen', '')
    comentario = request.POST.get('comentario', '')
    mascota = Mascota.objects.get(pk=id)
    mascota.nombre = nombre
    mascota.raza = raza
    mascota.estado = estado
    mascota.imagen = imagen
    mascota.comentario = comentario
    mascota.save()

    return redirect('mascotas')


def vista_modificar(request, id):
    mascota = Mascota.objects.get(id=id)
    print(mascota)
    return render(request, 'vista_modificar.html', {'mascota': mascota})


@login_required(login_url='login')
def adoptado(request):
    criterio = adoptado("estado: ")
    query = "SELECT estado FROM Mascota WHERE estado = 'Disponible'" % criterio
    result = adoptado(query)
    return result
