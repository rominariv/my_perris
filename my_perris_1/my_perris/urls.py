from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name="index"),
    path('registro/', views.registro, name="registro"),
    path('login/', views.login, name="login"),
    path('adoptantes/', views.adoptantes, name="adoptantes"),
    path('login/iniciar/',views.login_iniciar, name="iniciar"),
    path('mascotas/', views.mascotas, name="mascotas"),
    path('registro/crear/', views.crear, name="crear"),
    path('mascotas/crear_mascota/', views.crear_mascota, name="crear_mascota"),
    path('mascotas/modificar/<int:id>', views.modificar, name="modificar"),
    path('mascotas/vista_modificar/<int:id>', views.vista_modificar, name="vista_modificar"),
    path('mascotas/eliminar/<int:id>', views.eliminar, name="eliminar"),
    
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
