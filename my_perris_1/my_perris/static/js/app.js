//Oculta el formulario al abrir la página
$(function () {
	$("#formulario2").hide()
})
//Muestra el formulario2 (Mascotas) al presionar botón agregar
$("#mostrar2").on("click", function () {
	$("#formulario2").show()
})

//Oculta el formulario al hacer click en el botón "Ocultar"
$("#ocultar2").click(function () {
	$("#formulario2").hide()
})

//Oculta el formulario al hacer click en el botón "Ocultar"
$("#ocultar3").click(function () {
	$("#table1").hide()
})

//Oculta el formulario al hacer click en el botón "Ocultar"
$("#ocultar4").click(function () {
	$("#table2").hide()
})

$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})
$('.carousel').carousel()
//Validar formulario Registro
$(document).ready(function () {
	$(function () {
		$("#formulario").validate({
			rules: {
				nombre: {
					required: true,
					minlength: 5
				},

				run: {
					required: true,
					minlength: 9,
					run: true
				},

				fecha_naci: {
					required: true
				},

				correo: {
					required: true,
					email: true
				},

				celular: {
					required: false,
					min: 9
				},

				region: {
					required: true
				},

				ciudad: {
					required: true
				},

				casa: {
					required: true
				},

				password: {
					required: true
				},

			},
			messages: {
				nombre: {
					required: "Éste campo es obligatorio",
					minlength: "debe ingresar un min de 5 caractéres"
				},

				run: {
					required: "Éste campo es obligatorio",
					minlength: "Debe ingresar un rut válido",
					run: "Run incorrecto"
				},

				fecha_naci: {
					required: "Éste campo es obligatorio"
				},

				correo: {
					required: "Éste campo es obligatorio",
					email: "Debe ingresar un correo válido"
				},
				celular: {
					min: "Ingrese un número válido",

				},
				region: {
					required: "Debe seleccionar una Región"
				},
				ciudad: {
					required: "Debe seleccionar una Ciudad"
				},
				casa: {
					required: "Debe seleccionar un tipo de casa"
				},

				password: {
					required: "Debe ingresar una contraseña"
				}

			}
		})
	})



	//Validar regiones vs comunas
	//Ingresoar nombres de regiones y ciudades (comunas)
	var RegionesYcomunas = {

		"regiones": [{
			"NombreRegion": "Arica y Parinacota",
			"comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
		},
		{
			"NombreRegion": "Tarapacá",
			"comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
		},
		{
			"NombreRegion": "Antofagasta",
			"comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
		},
		{
			"NombreRegion": "Atacama",
			"comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
		},
		{
			"NombreRegion": "Coquimbo",
			"comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
		},
		{
			"NombreRegion": "Valparaíso",
			"comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
		},
		{
			"NombreRegion": "Región del Libertador Gral. Bernardo O’Higgins",
			"comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
		},
		{
			"NombreRegion": "Región del Maule",
			"comunas": ["Talca", "Constución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "ReVro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
		},
		{
			"NombreRegion": "Región del Biobío",
			"comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay"]
		},
		{
			"NombreRegion": "Región de la Araucanía",
			"comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria",]
		},
		{
			"NombreRegion": "Región de Los Ríos",
			"comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
		},
		{
			"NombreRegion": "Región de Los Lagos",
			"comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "FruVllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
		},
		{
			"NombreRegion": "Región Aisén del Gral. Carlos Ibáñez del Campo",
			"comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
		},
		{
			"NombreRegion": "Región de Magallanes y de la Antártica Chilena",
			"comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "AntárVca", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
		},
		{
			"NombreRegion": "Región Metropolitana de Santiago",
			"comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "Tiltil", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
		}]
	}

	//Función para parear los datos de región y comuna
	jQuery(document).ready(function () {

		var iRegion = 0;
		var htmlRegion = '<option value="sin-region">Seleccione una región</option><option value="sin-region">--</option>';
		var htmlComunas = '<option value="sin-region">Seleccione una ciudad</option><option value="sin-region">--</option>';

		jQuery.each(RegionesYcomunas.regiones, function () {
			htmlRegion = htmlRegion + '<option value="' + RegionesYcomunas.regiones[iRegion].NombreRegion + '">' + RegionesYcomunas.regiones[iRegion].NombreRegion + '</option>';
			iRegion++;
		});

		jQuery('#region').html(htmlRegion);
		jQuery('#ciudad').html(htmlComunas);

		jQuery('#region').change(function () {
			var iRegiones = 0;
			var valorRegion = jQuery(this).val();
			var htmlComuna = '<option value="sin-comuna">Seleccione una ciudad</option><option value="sin-comuna">--</option>';
			jQuery.each(RegionesYcomunas.regiones, function () {
				if (RegionesYcomunas.regiones[iRegiones].NombreRegion == valorRegion) {
					var iComunas = 0;
					jQuery.each(RegionesYcomunas.regiones[iRegiones].comunas, function () {
						htmlComuna = htmlComuna + '<option value="' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '">' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '</option>';
						iComunas++;
					});
				}
				iRegiones++;
			});
			jQuery('#ciudad').html(htmlComuna);
		});
		jQuery('#ciudad').change(function () {
			if (jQuery(this).val() == 'sin-region') {
				alert('Selecciona una Región');
			} else if (jQuery(this).val() == 'sin-comuna') {
				alert('Selecciona una ciudad');
			}
		});
		jQuery('#region').change(function () {
			if (jQuery(this).val() == 'sin-region') {
				alert('Selecciona una Región');
			}
		});

	});

	//Uso de plugin mask para escreibir el rut en formato Chileno de manera automática
	$(document).ready(function () {
		$('#run').mask('99.999.999-9');
	});

	//Uso de plugin mask para escribir el num celular con formato de manera automática
	$(document).ready(function () {
		$('#celular').mask('9 999 999 99');
	});

	//llamar función galeria SlipHover
	$(function () {
		window.prettyPrint && prettyPrint();

		$('.galery').sliphover();
	})
})
//Formulario para agregar mascotas
$(document).ready(function () {
	$(function () {
		$("#formulario2").validate({
			rules: {
				nombre: {
					required: true
				},

				raza: {
					required: true

				},

				estado: {
					required: true
				},

				imagen: {
					required: true
				},

				comentario: {
					required: true,
					maxlength: 50

				},
			},
			messages: {
				nombre: {
					required: "Éste campo es obligatorio"
				},

				raza: {
					required: "Éste campo es obligatorio"

				},

				estado: {
					required: "Éste campo es obligatorio"
				},

				imagen: {
					required: "Éste campo es obligatorio"
				},

				comentario: {
					required: "Éste campo es obligatorio",
					maxlength: "Ingresar máximo 0 caractéres"

				},
			}
		})
	})
})

$(function () {
    $("#form-log").validate({
        rules: {
            email:{
                required: true,   
            },
            contrasenia:{
                required: true
			}
        },
        messages: {
            email: {
                required: "Este campo es obligatorio",
				email: "Debe ingresar un formato de correo correcto"
            },
            contrasenia: {
                required: "Este campo es obligatorio",
			}
        }
    })
})